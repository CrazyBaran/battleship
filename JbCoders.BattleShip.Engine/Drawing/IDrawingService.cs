﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Drawing
{
    public interface IDrawingService
    {
        string Draw(IEnumerable<(int x, int y)> hits);
    }
}
