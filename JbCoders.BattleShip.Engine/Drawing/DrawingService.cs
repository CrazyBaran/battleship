﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Drawing
{
    public class DrawingService : IDrawingService
    {
        const int boardSize = 10;
        public string Draw(IEnumerable<(int x, int y)> hits)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"  ");
            for (int i = 0; i < boardSize; i++)
            {
                stringBuilder.Append($" {i + 1}");
            }
            stringBuilder.Append(Environment.NewLine);
            for (int i = 0; i < boardSize; i++)
            {
                stringBuilder.Append($" {(char)(i + 65)}");
                for (int j = 0; j < boardSize; j++)
                {
                    stringBuilder.Append(hits.Any(_ => _ == (i, j)) ? " X" : " ~");
                }
                stringBuilder.Append(Environment.NewLine);
            }
            return stringBuilder.ToString();
        }
    }
}
