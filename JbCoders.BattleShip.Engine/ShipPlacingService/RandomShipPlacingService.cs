﻿using JbCoders.BattleShip.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.ShipPlacingService
{
    public class RandomShipPlacingService : IShipPlacingService
    {
        private Random _random;

        public RandomShipPlacingService()
        {
            _random = new Random();
        }

        public Dictionary<(int x, int y), ShipPart> GetShipParts(IEnumerable<int> shipLengths, int boardSize = 10)
        {
            var shipParts = new Dictionary<ValueTuple<int, int>, ShipPart>();
            foreach (var shipLength in shipLengths)
            {
                List<(int, int)> positions = null;
                do
                {
                    var randomIsHorizontal = _random.Next() % 2 == 0;
                    var randomPosition = randomIsHorizontal ?
                        (_random.Next() % boardSize, _random.Next() % (boardSize - shipLength)) :
                        (_random.Next() % (boardSize - shipLength), _random.Next() % boardSize);
                    positions = ShipPlaceHelper.GeneratePositions(randomPosition,
                        randomIsHorizontal,
                        shipLength);
                } while (!ShipPlaceHelper.IsPossibleToPlaceShip(shipParts.Select(_ => _.Key), positions, boardSize));

                var ship = new Ship(positions.Count);
                foreach (var position in positions)
                {
                    shipParts.Add(position, new ShipPart(ship));
                }
            }

            return shipParts;
        }
    }
}
