﻿using JbCoders.BattleShip.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.ShipPlacingService
{
    public interface IShipPlacingService
    {
        Dictionary<(int x, int y), ShipPart> GetShipParts(IEnumerable<int> shipLengths, int boardSize = 10);
    }
}
