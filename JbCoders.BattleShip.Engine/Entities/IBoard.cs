﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public interface IBoard
    {
        BoardResult Fire((int, int) coordinate);
    }
}
