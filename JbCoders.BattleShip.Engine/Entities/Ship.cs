﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class Ship
    {
        private int _length;
        public Ship(int length)
        {
            _length = length;
        }

        public bool Fire()
        {
            if(_length != 0)
            {
                _length--;
            }

            return _length < 1;
        }
    }
}
