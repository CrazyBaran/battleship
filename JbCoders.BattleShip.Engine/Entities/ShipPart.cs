﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class ShipPart
    {
        private Ship _ship;

        public ShipPart(Ship ship)
        {
            _ship = ship;
        }

        public bool IsFired { get; private set; }

        public bool Fire()
        {
            if(IsFired == false)
            {
                IsFired = true;
                return _ship.Fire();
            }

            return false;
        }
    }
}
