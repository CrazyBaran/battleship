﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public interface IBoardFactory
    {
        Board BuildBoard(IEnumerable<int> shipLengths, int boardSize = 10);
    }
}
