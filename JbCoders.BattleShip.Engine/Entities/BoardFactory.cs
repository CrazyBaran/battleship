﻿using JbCoders.BattleShip.Engine.ShipPlacingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class BoardFactory : IBoardFactory
    {
        private readonly IShipPlacingService _shipPlacingService;
        public BoardFactory(IShipPlacingService shipPlacingService)
        {
            _shipPlacingService = shipPlacingService;
        }
        public Board BuildBoard(IEnumerable<int> shipLengths, int boardSize = 10)
        {


            return new Board(_shipPlacingService.GetShipParts(shipLengths, boardSize));
        }
    }
}
