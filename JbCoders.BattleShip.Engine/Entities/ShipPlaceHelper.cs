﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public static class ShipPlaceHelper
    {
        public static bool IsPossibleToPlaceShip(IEnumerable<(int X, int Y)> takenPlaces, IEnumerable<(int X, int Y)> shipPosition, int boardSize)
        {
            if (shipPosition.Any(_ => _.X >= boardSize || _.X < 0 || _.Y >= boardSize || _.Y < 0))
            {
                return false;
            }

            if (shipPosition.Any(sp => IsInsideOfTakenPlacesZone(takenPlaces, sp)))
            {
                return false;
            }

            return true;
        }

        private static bool IsInsideOfTakenPlacesZone(IEnumerable<(int X, int Y)> takenPlaces, (int X, int Y) position)
        {
            return takenPlaces.Any(_ =>  (Math.Abs(_.X - position.X) < 2) && (Math.Abs(_.Y - position.Y) < 2));
        }

        public static List<(int X, int Y)> GeneratePositions((int X, int Y) startPosition, bool isHorizontal, int length)
        {
            var positions = new List<(int, int)>();
            
            for(int i = 0; i < length; i++)
            {
                positions.Add(isHorizontal 
                    ? (startPosition.X, startPosition.Y + i) 
                    : (startPosition.X + i, startPosition.Y));
            }

            return positions;
        }
    }
}
