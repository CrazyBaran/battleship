﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class BoardResult
    {
        public string Result { get; set; }
        public IEnumerable<ValueTuple<int, int>> Hits { get; set; }
    }
}
