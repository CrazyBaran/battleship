﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JbCoders.BattleShip.Engine.Entities
{
    public class Board : IBoard
    {
        private readonly Dictionary<ValueTuple<int, int>, ShipPart> _shipParts;
        public Board(Dictionary<(int, int), ShipPart> shipParts)
        {
            _shipParts = shipParts;
        }

        public BoardResult Fire((int, int) coordinate)
        {
            var boardResult = new BoardResult()
            {
                Result = "MISS"
            };

            if (_shipParts.TryGetValue(coordinate, out var shipPart) && shipPart.IsFired != true)
            {
                boardResult.Result = "HIT";
            };

            if (shipPart?.Fire() == true)
            {
                boardResult.Result = "SUNK";
            };

            if(_shipParts.All(_=>_.Value.IsFired))
            {
                boardResult.Result = "END";
            }

            boardResult.Hits = _shipParts.Where(_ => _.Value.IsFired).Select(_ => _.Key);

            return boardResult;
        }
    }
}
