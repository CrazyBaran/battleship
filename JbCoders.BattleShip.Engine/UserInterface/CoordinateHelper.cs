﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace JbCoders.BattleShip.Engine.UserInterface
{
    public static class CoordinateHelper
    {
        public static bool TryParseCoordinate(string textCoordinate, out ValueTuple<int, int>? coordinate)
        {
            coordinate = null;
            textCoordinate = String.Concat(textCoordinate.Where(c => !Char.IsWhiteSpace(c)));
            Regex rgx = new Regex("^([a-jA-J])([0-9]|10)$");

            if (rgx.IsMatch(textCoordinate))
            {
                textCoordinate = textCoordinate.ToUpper();
                var letter = textCoordinate[0];
                coordinate = (letter - 'A', int.Parse(textCoordinate[1..]) - 1);
                return true;
            }
            return false;
        }
    }
}
