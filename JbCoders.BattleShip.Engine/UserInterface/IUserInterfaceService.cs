﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.UserInterface
{
    public interface IUserInterfaceService
    {
        void Show(string content);

        string ReadInput();
    }
}
