﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Game
{
    public interface IGameService
    {
        void Play();
    }
}
