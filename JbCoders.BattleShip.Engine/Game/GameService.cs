﻿using JbCoders.BattleShip.Engine.Drawing;
using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.UserInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.Engine.Game
{
    public class GameService : IGameService
    {
        private readonly IUserInterfaceService _uiService;
        private readonly IDrawingService _drawingService;
        private readonly IBoard _board;
        public GameService(IBoard board, IUserInterfaceService uiService, IDrawingService drawingService)
        {
            _board = board;
            _uiService = uiService;
            _drawingService = drawingService;
        }

        public void Play()
        {
            _uiService.Show(_drawingService.Draw(Array.Empty<(int x, int y)>()));
            while(true)
            {
                var input = _uiService.ReadInput();
                if (input == "X")
                {
                    _uiService.Show("END");
                    break;
                }

                if(!CoordinateHelper.TryParseCoordinate(input, out var coordinate))
                {
                    continue;
                }

                var result = _board.Fire(coordinate.Value);

                _uiService.Show($"{_drawingService.Draw(result.Hits)}" +
                    Environment.NewLine +
                    result.Result);

                if(result.Result == "END")
                {
                    break;
                }
            }
        }
    }
}
