﻿using JbCoders.BattleShip.Engine.Drawing;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace JbCoders.BattleShip.EngineUnitTests
{
    public class DrawingServiceTests
    {
        [Fact]
        public void Should_Draw_Only_Water()
        {
            var drawingService = new DrawingService();

            var result = drawingService.Draw(Array.Empty<ValueTuple<int, int>>());

            Assert.Equal("   1 2 3 4 5 6 7 8 9 10\r\n" +
                         " A ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " B ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " C ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " D ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " E ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " F ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " G ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " H ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " I ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " J ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n",
                         result);
        }

        [Fact]
        public void Should_Draw_Two_Hits_On_A5_B2()
        {
            var drawingService = new DrawingService();

            var result = drawingService.Draw(new List<(int, int)> { (0, 4), (1, 1) });

            Assert.Equal("   1 2 3 4 5 6 7 8 9 10\r\n" +
                         " A ~ ~ ~ ~ X ~ ~ ~ ~ ~\r\n" +
                         " B ~ X ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " C ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " D ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " E ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " F ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " G ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " H ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " I ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n" +
                         " J ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\r\n",
                         result);
        }
    }
}
