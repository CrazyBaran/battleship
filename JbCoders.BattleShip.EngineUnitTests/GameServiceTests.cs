using JbCoders.BattleShip.Engine.Drawing;
using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.Game;
using JbCoders.BattleShip.Engine.UserInterface;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace JbCoders.BattleShip.EngineTests
{
    public class GameServiceTests
    {
        [Fact]
        public void Game_Should_End_When_X_Pressed()
        {
            var uiServiceMock = new Mock<IUserInterfaceService>();
            uiServiceMock.Setup(_ => _.ReadInput()).Returns("X");

            var drawingServiceMock = new Mock<IDrawingService>();
            drawingServiceMock.Setup(_ => _.Draw(It.IsAny<IEnumerable<(int, int)>>())).Returns("");

            var boardMock = new Mock<IBoard>();

            var game = new GameService(boardMock.Object, uiServiceMock.Object, drawingServiceMock.Object);

            game.Play();

            uiServiceMock.Verify(_ => _.ReadInput(), Times.Once);
            uiServiceMock.Verify(_ => _.Show("END"), Times.Once);
        }


    }
}