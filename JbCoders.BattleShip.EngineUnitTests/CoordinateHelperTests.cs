﻿using JbCoders.BattleShip.Engine.UserInterface;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace JbCoders.BattleShip.EngineUnitTests
{
    public class CoordinateHelperTests
    {
        [Theory]
        [InlineData("A1", 0, 0)]
        [InlineData("A   1", 0, 0)]
        [InlineData("    A   1", 0, 0)]
        [InlineData("B7", 1, 6)]
        [InlineData("j10", 9, 9)]
        [InlineData("A10", 0, 9)]
        public void Should_Parse_Correctly(string text, int coordinateX, int coordinateY)
        {
            var result = CoordinateHelper.TryParseCoordinate(text, out var coordinate);

            Assert.True(result);
            Assert.Equal((coordinateX, coordinateY), coordinate);
        }

        [Theory]
        [InlineData("A")]
        [InlineData("100A")]
        [InlineData("A1A")]
        [InlineData(":1")]
        [InlineData("W100")]
        public void Should_Fail_Parse(string text)
        {
            var result = CoordinateHelper.TryParseCoordinate(text, out var coordinate);

            Assert.False(result);
            Assert.Null(coordinate);
        }
    }
}
