﻿using JbCoders.BattleShip.Engine.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace JbCoders.BattleShip.EngineUnitTests
{
    public class ShipPlaceHelperTests
    {
        [Theory]
        [InlineData(10, 4, 4)]
        [InlineData(10, 4, 5)]
        [InlineData(10, 4, 6)]
        [InlineData(10, 5, 4)]
        [InlineData(10, 5, 5)]
        [InlineData(10, 5, 6)]
        [InlineData(10, 6, 4)]
        [InlineData(10, 6, 5)]
        [InlineData(10, 6, 6)]
        [InlineData(10, 10, 6)]
        [InlineData(10, 6, 10)]
        [InlineData(10, -1, 5)]
        [InlineData(10, 9, -1)]
        [InlineData(10, 5, 3, 5, 4)]
        public void Should_Not_Be_Possible_To_Place_Ship(int boardSize, params int[] coordinate)
        {
            var shipPosition = new List<(int, int)>();
            if (coordinate.Length > 1 && coordinate.Length % 2 == 0)
            {
                for (int i = 0; i < coordinate.Length; i += 2)
                {
                    shipPosition.Add((coordinate[i], coordinate[i + 1]));
                }
            }
            else
            {
                throw new Exception("Wrongly setup of InlineData");
            }

            var result = ShipPlaceHelper.IsPossibleToPlaceShip(new (int, int)[] { (5, 5) }, shipPosition, boardSize);

            Assert.False(result);
        }

        [Theory]
        [InlineData(10, 3, 3)]
        [InlineData(10, 3, 4)]
        [InlineData(10, 3, 5)]
        [InlineData(10, 3, 6)]
        [InlineData(10, 3, 7)]
        [InlineData(10, 4, 3)]
        [InlineData(10, 5, 3)]
        [InlineData(10, 6, 3)]
        [InlineData(10, 7, 3)]
        [InlineData(10, 7, 7)]
        [InlineData(10, 6, 7)]
        [InlineData(10, 5, 7)]
        [InlineData(10, 4, 7)]
        [InlineData(10, 0, 0)]
        [InlineData(10, 9, 9)]
        [InlineData(10, 0, 9)]
        [InlineData(10, 9, 0)]
        public void Should_Be_Possible_To_Place_Ship(int boardSize, params int[] coordinate)
        {
            var shipPosition = new List<(int, int)>();
            if (coordinate.Length > 1 && coordinate.Length % 2 == 0)
            {
                for (int i = 0; i < coordinate.Length; i += 2)
                {
                    shipPosition.Add((coordinate[i], coordinate[i + 1]));
                }
            }
            else
            {
                throw new Exception("Wrongly setup of InlineData");
            }

            var result = ShipPlaceHelper.IsPossibleToPlaceShip(new (int, int)[] { (5, 5) }, shipPosition, boardSize);

            Assert.True(result);
        }

        [Fact]
        public void Should_Generate_5_Position()
        {
            var result = ShipPlaceHelper.GeneratePositions((0, 0), true, 5);

            Assert.Equal(5, result.Count);
        }

        [Fact]
        public void Should_Generate_Horizontal_Position()
        {
            var result = ShipPlaceHelper.GeneratePositions((0, 0), true, 5);

            Assert.Equal(new List<(int, int)> { (0, 0), (0, 1), (0, 2), (0, 3), (0, 4) }, result);
        }

        [Fact]
        public void Should_Generate_Vertical_Position()
        {
            var result = ShipPlaceHelper.GeneratePositions((0, 0), false, 5);

            Assert.Equal(new List<(int, int)> { (0, 0), (1, 0), (2, 0), (3, 0), (4, 0) }, result);
        }


    }
}
