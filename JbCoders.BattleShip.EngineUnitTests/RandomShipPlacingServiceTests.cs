﻿using JbCoders.BattleShip.Engine.ShipPlacingService;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace JbCoders.BattleShip.EngineUnitTests
{
    public class RandomShipPlacingServiceTests
    {

        [Theory]
        [InlineData(1, 1)]
        [InlineData(2, 1, 1)]
        [InlineData(3, 2, 1)]
        [InlineData(13, 4, 4, 5)]
        public void Should_Generate_Expected_Number_Of_ShipPart(int shipPartCount, params int[] shipLengths)
        {
            var randomShipPlacingService = new RandomShipPlacingService();

            var result = randomShipPlacingService.GetShipParts(shipLengths);

            Assert.Equal(shipPartCount, result.Count);
        }
    }
}
