﻿using JbCoders.BattleShip.Engine.Drawing;
using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.Game;
using JbCoders.BattleShip.Engine.ShipPlacingService;
using JbCoders.BattleShip.Engine.UserInterface;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace JbCoders.BattleShip.EngineFunctionalTests
{
    public class GameServiceTests
    {
        [Fact]
        public void Firing_All_Location_One_By_One()
        {
            var uiServiceMock = new Mock<IUserInterfaceService>();

            var drawingService = new DrawingService();

            var boardFactory = new BoardFactory(new RandomShipPlacingService());
            var board = boardFactory.BuildBoard(new int[] { 5, 4, 4 });

            int letter = 65, number = 0, executionCount = 0;
            uiServiceMock.Setup(_ => _.ReadInput()).Returns(() =>
            {
                executionCount++;
                if (executionCount > 101)
                {
                    Assert.True(false, "Game should end already");
                }
                number++;

                if (number == 11)
                {
                    number = 1;
                    letter++;
                }
                return $"{(char)letter}{number}";
            });

            var game = new GameService(board, uiServiceMock.Object, drawingService);

            game.Play();

            uiServiceMock.Verify(_ => _.ReadInput(), Times.AtLeast(13));
            uiServiceMock.Verify(_ => _.Show(It.Is<string>(_ => _.EndsWith("MISS"))), Times.AtLeastOnce());
            uiServiceMock.Verify(_ => _.Show(It.Is<string>(_ => _.EndsWith("HIT"))), Times.Exactly(10));
            uiServiceMock.Verify(_ => _.Show(It.Is<string>(_ => _.EndsWith("SUNK"))), Times.Exactly(2));
            uiServiceMock.Verify(_ => _.Show(It.Is<string>(_ => _.EndsWith("END"))), Times.Once);
        }
    }
}
