﻿using JbCoders.BattleShip.Engine.UserInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace JbCoders.BattleShip.ConsoleApp
{
    public class ConsoleUserInterfaceService : IUserInterfaceService
    {
        public string ReadInput()
        {
            return Console.ReadLine();
        }

        public void Show(string content)
        {
            Console.Clear();
            Console.WriteLine(content);
        }
    }
}
