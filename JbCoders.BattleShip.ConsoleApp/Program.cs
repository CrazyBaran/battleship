﻿using JbCoders.BattleShip.Engine.Drawing;
using JbCoders.BattleShip.Engine.Entities;
using JbCoders.BattleShip.Engine.Game;
using JbCoders.BattleShip.Engine.ShipPlacingService;
using JbCoders.BattleShip.Engine.UserInterface;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace JbCoders.BattleShip.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IGameService, GameService>()
                .AddSingleton<IUserInterfaceService, ConsoleUserInterfaceService>()
                .AddSingleton<IBoardFactory, BoardFactory>()
                .AddSingleton<IShipPlacingService, RandomShipPlacingService>()
                .AddSingleton<IDrawingService, DrawingService>()
                .AddSingleton<IBoard>(sp => {
                    var boardFactory = sp.GetService<IBoardFactory>();
                    return boardFactory.BuildBoard(new int[] { 5, 4, 4 });
                })
                .BuildServiceProvider();

            var game = serviceProvider.GetService<IGameService>();
            game.Play();
        }
    }
}
