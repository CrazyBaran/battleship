Recruitment Test: Battleships
=============================

Requirements
--------

Can be find here: https://medium.com/guestline-labs/hints-for-our-interview-process-and-code-test-ae647325f400

How to run
--------
Execute following commands to run the game.
``` bash
git clone git clone https://CrazyBaran@bitbucket.org/CrazyBaran/battleship.git
cd Battleship
dotnet run -p JbCoders.BattleShip.ConsoleApp
```

How to test
--------
``` bash
git clone git clone https://CrazyBaran@bitbucket.org/CrazyBaran/battleship.git
cd Battleship
dotnet test 
```

License
--------
The MIT License (MIT)